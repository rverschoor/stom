class Download

  attr_reader :filename

  def initialize(name, url)
    @filename = nil
    secrets
    @conn = Faraday.new(
      params: {token: @token},
      headers: {'Cookie' => "d=#{@cookie}"}
    )
    download url
  end

  def secrets
    `op signin`
    @cookie = `op read "op://Private/stom/secrets/cookie"`.strip
    @token = `op read "op://Private/stom/secrets/token"`.strip
  end

  def download(url)
    resp = @conn.get(url)
    if resp.status == 200
      @filename = File.basename(url)
      File.open(@filename, 'wb') { |fp| fp.write(resp.body) }
    end
  end

end