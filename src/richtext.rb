require_relative 'richtextsection'

class RichText

  attr_reader :data

  def initialize(elements)
    @data = []
    elements.each do |element|
      type = element['type']
      if type == 'rich_text_section'
        @data.concat (RichTextSection.new element['elements']).data
      elsif type == 'rich_text_list'
        @data << {warning: 'rich_text_list not yet implemented'}
      else
        @data << {warning: "Rich text element type #{type} not implemented"}
      end
    end
    @data
  end

end