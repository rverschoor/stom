class RichTextSection

attr_accessor :data

  def initialize(elements)
    @data = []
    elements.each do |element|
      type = element['type']
      case type
      when 'text'
        text_element element
      when 'emoji'
        emoji_element element
      when 'link'
        link_element element
      when 'user'
        user_element element
      else
        @data << {warning: "Rich-text section type #{type} not implemented"}
      end
    end
    @data
  end

  def text_element(element)
    text = element['text']
    style = element['style']
    @data << {text: {text: text, style: style}}
  end

  def emoji_element(element)
    unicode = element['unicode']
    if unicode
      @data << {emoji: unicode}
    else
      custom_emoji_element element
    end
  end

  def custom_emoji_element(element)
    @data << {custom_emoji: element['name']}
  end

  def link_element(element)
    url =  element['url']
    text = element['text']
    @data << {link: {text: text, url: url}}
  end

  def user_element(element)
    user_id = element['user_id']
    @data << {user: user_id}
  end

end