class Files

  attr_accessor :data

  def initialize(files)
    @data = []
    files.each do |file|
      @data << {file: {title: file['title'], url: file['url_private_download']}}
    end
  end

end