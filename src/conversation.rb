require_relative 'permalink'
require_relative 'comment'
require_relative 'markdown'

class Conversation

  def initialize(api, host, url)
    @api = api
    @permalink = Permalink.new host, url
    channelname = get_channelname
    get_conversation
    @md = Markdown.new @api
    @md.channelname channelname
    process_conversation
    puts
  end

  def get_channelname
    info = @api.call 'conversations.info', { channel: @permalink.channel }
    info['channel']['name']
  end

  def get_conversation
    @conversation = @api.call 'conversations.replies', { channel: @permalink.channel, ts: @permalink.ts }
  end

  def process_conversation
    @conversation['messages'].each do |message|
      render (Comment.new @api, message).data
    end
  end

  def render(data)
    @md.render(data)
  end

end

