class Image

  def initialize(block)
    @data = []
    url = block['image_url']
    alt = block['alt_text']
    title = block['title']['text']
    @data << "IMAGE: title=#{title} alt=#{alt} url=#{url}"
    @data
  end

end