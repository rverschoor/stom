require 'faraday'
require 'json'

class Api
  
  def initialize(cookie, token)
    @conn = Faraday.new(
      url: 'https://slack.com/api',
      params: {token: token},
      headers: {'Cookie' => "d=#{cookie}"}
    ) do |faraday|
      faraday.response :json
    end
  end

  def call(endpoint, params)
    resp = @conn.get(endpoint, params)
    handle_response resp
  end

  def handle_response(resp)
    if resp.status != 200
      puts "Failure #{resp.status}"
      exit
    end
    if resp.body['error']
      puts "Error #{resp.body['error']}"
      exit
    end
    if resp.body['warning']
      puts "Warning #{resp.body['warning']}"
    end
    resp.body
  end
  
end
