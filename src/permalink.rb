require 'uri'

class Permalink

  attr_reader :channel
  attr_reader :ts

  def initialize(host, url)
    @host = host
    process url
  end

  def process(url)
    # path: /archives/C0FGWFRMX/p1678709567859619
    # => ts is given, but without dot
    #
    # path: /archives/C0FGWFRMX/p1678709689169859
    # query: thread_ts=1678709567.859619&cid=C0FGWFRMX
    # => ts is given in thread_ts, which includes the dot
    uri = URI(url)
    if uri.host != @host or not uri.path.start_with?('/archives/')
      puts 'Not a Slack URL'
      exit
    end
    if uri.query
      # query: thread_ts=1678709567.859619&cid=C0FGWFRMX
      match = uri.query.match(/thread_ts=(\d+\.\d+)&cid=(.*)/)
      @ts = match[1]
      @channel = match[2]
    else
      # path: /archives/C0FGWFRMX/p1678709567859619
      match = uri.path.match(/\/archives\/(C.*)\/(p\d{10})(\d{6})/)
      @channel = match[1]
      @ts = match[2] + '.' + match[3]
    end
  end

end
