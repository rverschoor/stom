require_relative 'block'
require_relative 'files'

class Comment

  attr_reader :data

  def initialize(api, message)
    @data = []
    @api = api
    @message = message
    read
  end

  def read
    @data << {author: @message['user']}
    @data << {timestamp: @message['ts']}
    type = @message['type']
    if type == 'message'
      if @message['files']
        @data.concat (Files.new @message['files']).data
      end
      if @message['attachments']
        @data << {warning: 'attachments not yet implemented'}
      end
      @message['blocks'].each do |block|
        @data.concat (Block.new block).data
      end
    else
      @data << {warning: "conversation type #{type} not implemented"}
    end
  end

end
