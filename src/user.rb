class User

  def initialize(api, user_id)
    @api = api
    lookup user_id
  end

  def lookup(user_id)
    @info = @api.call 'users.info', { user: user_id }
  end

  def name
    @info['user']['name']
  end

  def real_name
    @info['user']['real_name']
  end

end