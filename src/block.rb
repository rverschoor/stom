require_relative 'richtext'
require_relative 'image'

class Block

  attr_reader :data

  def initialize(block)
    @data = []
    @block = block
    show
  end

  def show
    type = @block['type']
    if type == 'rich_text'
      @data.concat (RichText.new @block['elements']).data
    elsif type == 'image'
      @data.concat (Image.new @block).data
    elsif type == 'context'
      context
    else
      @data << {warning: "Block type #{type} not implemented"}
    end
  end

  def context
    # can be ignored I think
  end

end
