require_relative 'user'
require_relative 'download'

class Markdown

  def initialize(api)
    @api = api
  end

  def render(data)
    data.each do |element|
      element.each do |key, value|
        case key
        when :author
          author value
        when :custom_emoji
          custom_emoji value
        when :emoji
          emoji value
        when :file
          file value
        when :link
          link value
        when :text
          text value
        when :timestamp
          timestamp value
        when :user
          user value
        when :warning
          warning value
        else
          warning "unknown render key #{key}"
        end
      end
    end
  end

  def channelname(name)
    puts "##{name} \\"
  end

  def author(value)
    user = User.new @api, value
    print "\\\n💬 #{user.real_name} (#{user.name}) "
  end

  def custom_emoji(value)
    warning 'MD custom_emoji not yet implemented'
  end

  def emoji(value)
    print ["#{value}".hex].pack("U")
  end

  def file(value)
    download = Download.new value[:title], value[:url]
    puts "![#{value[:title]}](#{download.filename})"
  end

  def link(value)
    if value[:text]
      print "[#{value[:text]}](#{value[:url]})"
    else
      print "#{value[:url]}"
    end
  end

  def text(value)
    text = value[:text]
    style = value[:style]
    md_style style if style
    print text
    md_style style.reverse_each.to_h if style
  end

  def timestamp(value)
    utc = ts_to_utc value
    puts "#{utc} \\"
  end

  def user(value)
    user = User.new @api, value
    print "@#{user.name}"
  end

  def warning(value)
    puts "\\\n🔥 #{value} \\"
  end

  def ts_to_utc(ts)
    stamp = ts.split('.', 2)
    DateTime.strptime(stamp.first, '%s').strftime('%Y-%m-%d %H:%M')
  end

  def md_style(style)
    style.each do |key, value|
      case key
      when 'bold'
        print '**' if value
      when 'italic'
        print '_' if value
      when 'code'
        print '`' if value
      when 'strike'
        print '~~' if value
      else
        warning "markdown style #{key} not implemented"
      end
    end
  end

end

