#!/usr/bin/env ruby

require_relative 'src/api'
require_relative 'src/conversation'

class Stom

  def initialize
    arguments
    secrets
    api
    Conversation.new @api, @host, @url
  end

  def arguments
    @url = ARGV[0]
  end

  def secrets
    `op signin`
    @host = `op read "op://Private/stom/secrets/host"`.strip
    @cookie = `op read "op://Private/stom/secrets/cookie"`.strip
    @token = `op read "op://Private/stom/secrets/token"`.strip
  end

  def api
    @api = Api.new @cookie, @token
  end

end

Stom.new
